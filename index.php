<?php
  $saved_movies = array_diff(scandir('data/comments'), array('.', '..'));
  $i = 0;
  foreach ($saved_movies as $saved_movie) {
    $file_movie = fopen("data/comments/$saved_movie", 'r');
    $movies[$i]['id'] = $saved_movie;
    $movies[$i]['title'] = fgets($file_movie);
    fclose($file_movie);
    $i++;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <?php require('parts/head.php'); ?>

  <title>Films enregistrés</title>
</head>
<body>
  <?php require('parts/navbar.php'); ?>
  
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-8 mx-auto">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Titre</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($movies as $movie){ ?>
              <tr>
                <td><a href="fiche.php?id=<?php echo $movie['id']; ?>"><?php echo $movie['id']; ?></a></td>
                <td><?php echo $movie['title']; ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>
</html>