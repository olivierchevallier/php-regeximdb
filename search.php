<?php 
  $search = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';
  $search_q = str_replace(' ', '+', $search);
  $search_content = file_get_contents("https://www.imdb.com/find?q=$search_q");
  
  $regex_get_search_table = "/<table class=\"findList\">(?s)(.*)<\/table>/";
  preg_match($regex_get_search_table, $search_content, $match);
  $search_html_table = $match[1];
  $regex_get_movies_infos = "/<td\sclass=\"result_text\">\s<a\shref=\"\/title\/((?:\d|\w)*)\/\?ref_=fn_al_tt_\d*\"\s>((?:\w|\d|\s|[:\.,'àéîèô])*)</";
  preg_match_all($regex_get_movies_infos, $search_html_table, $matches);
  for($i = 0 ; $i < sizeof($matches[1]) ; $i++) {
    $films[$i][0] = $matches[1][$i];
    $films[$i][1] = $matches[2][$i];
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <?php require('parts/head.php'); ?>

  <title>Recherche de film</title>
</head>
<body>
  <?php require('parts/navbar.php'); ?>
  
  <div class="container pt-5">
    <div class="row">
      <h1 class="h3">Résultats de la recherche pour : <?php echo $search; ?></h1>
      <ul>
        <?php foreach($films as $film) { ?>
          <li><a href="fiche.php?id=<?php echo $film[0]; ?>"><?php echo $film[1]; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
</body>
</html>