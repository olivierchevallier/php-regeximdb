<?php 
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';

$page_content = file_get_contents('https://www.imdb.com/title/' . $id, 'r');

// Get title and year
$regex_get_title_plus_year = "/<title>((?:\w|\d|\s|[:\.,'àéîèô!])*)\((?:TV\sMovie\s){0,1}(\d{4})\)/";
preg_match($regex_get_title_plus_year, $page_content, $match);
$title = $match[1];
$year = $match[2];

// Get rating
$regex_get_rating = "/ratingValue\">(\d(?:,|.){0,1}\d{0,1})/";
preg_match($regex_get_rating, $page_content, $match);
$rating = isset($match[1]) ? $match[1] : '-';

// Get actors
$regex_get_actors_table = "/<table class=\"cast_list\">(?s)(.*)<\/table>/";
preg_match($regex_get_actors_table, $page_content, $match);
$actors_html_table = $match[1];
$regex_get_actors_name = "/<a\shref=\"\/name\/nm\d*\/\?ref_=tt_cl_t\d*\"\n>\s(.*)/";
preg_match_all($regex_get_actors_name, $actors_html_table, $matches);
$actors = $matches[1];

// Get poster
$regex_get_poster = "/Poster\"\ssrc=\"(.*)\"/";
preg_match($regex_get_poster, $page_content, $match);
$poster = $match[1];

// Get comment
$comment_file = @fopen("data/comments/$id", "r");
$comment = '';
if ($comment_file) {
  fgets($comment_file);
  while ($data = fgets($comment_file)) {
      $comment .= $data;
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <?php require('parts/head.php'); ?>
  <title>Fiche de film</title>
</head>
<body>
  <?php require('parts/navbar.php'); ?>

  <div class="container pt-5">
    <div class="row">
      <div class="col-4 col-xl-2">
        <img src="<?php echo $poster; ?>" class="card-img-top" alt="Affiche du film">
      </div>
      <div class="col-8 col-xl-5">
        <h1 class="h2"><?php echo $title; ?></h1>
        <span class="h4"><?php echo $year; ?> |</span><span class="h6"> <i class="fas fa-star text-warning"></i> <?php echo $rating; ?>/10</span>
        <h6 class="mt-4">Commentaire personnel</h6>
        <form action="savecomment.php" class="d-grid gap-2">
          <input type="hidden" name="id" value="<?php echo $id; ?>">
          <input type="hidden" name="title" value="<?php echo $title; ?>">
          <textarea name="comment" class="form-control"  id="comment" placeholder="Pas de commentaire enregistré pour le moment."><?php echo $comment ? $comment : ''; ?></textarea>
          <input class="btn btn-primary" type="submit" value="Enregistrer">
        </form>
      </div>
      <div class="col-12 col-md-6 offset-md-3 col-xl-5 offset-xl-0">
        <table class="table table-striped table-sm text-center">
          <thead>
          <tr>
            <th>Acteurs</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach($actors as $actor){ ?>
            <tr>
              <td><?php echo $actor; ?></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>