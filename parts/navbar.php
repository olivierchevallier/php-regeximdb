<nav class="navbar navbar-dark bg-primary navbar-expand-md">
    <div class="container-fluid">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <span class="navbar-brand mt-0 mb-0 h1">OliCiné</span>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Liste des films enregistrés</a>
          </li>
        </ul>
        <form method="GET" action="search.php" class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Rechercher" aria-label="Search" name="q">
          <button class="btn btn-outline-light" type="submit"><i class="fas fa-search"></i></button>
        </form>
      </div>
    </div>
  </nav>